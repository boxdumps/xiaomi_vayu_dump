## qssi-user 11 RKQ1.200826.002 V12.5.7.0.RJUMIXM release-keys
- Manufacturer: xiaomi
- Platform: msmnile
- Codename: vayu
- Brand: Xiaomi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.200826.002
- Incremental: V12.5.7.0.RJUMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Xiaomi/vayu_global/vayu:11/RKQ1.200826.002/V12.5.7.0.RJUMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.200826.002-V12.5.7.0.RJUMIXM-release-keys-random-text-22748435220012
- Repo: xiaomi_vayu_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
